Android Package Installer(ver 3.0) created by SAI PRASAD 
A simple lightweight GUI tool to install Android apks from your Windows, Mac or Linux Computers directly to your Android phone or Tablet. 

________About the software__________ 

This software is totally written in Java uses JavaFX and requires a user to Install a latest version of JDK and minimum jdk version required is JDK 8. 

_Pre-requisites_ 

1 - Minimum version JDK 1.8(if you don't have JDK then get the latest version from the Oracle site) 
2 - 128 MB RAM 
3 - Pentium IV CPU (Now-a-days its an old age processor you don't need to worry about it.) 

____Features____ 
1 - Works on Mac, Windows and Linux.(Tested and ran successfully) 
2 - Simplistic GUI and lightweight 
3 - Cross Platform software(as shown in above) 
4 - Shows the process output in the console area. 
5 - Its a free software (I'm releasing it under Creative Commons Attribution-NonCommercial 4.0 International License.) 

Release Notes (v3.0):
1- Improved UserInterface.
2- Switched from swing to JavaFX


Release Notes ( v2.0 ): 
1 - Bug fixed enabling the support with Linux and Mac 
2 - Added Android ADB script(created by kalaker a XDA-DEVELOPERS member) present in the Android ADB.zip file for Mac and Linux to install adb. 

--Thanks -SAI PRASAD